package com.shashankk.github_pr.api

import com.shashankk.github_pr.model.PullRequest
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GitHubApi {
    @GET("/repos/{user}/{repo}/pulls")
    fun getPullRequests(@Path("user") user : String, @Path("repo") repo : String, @Query("page") page: Int,@Query("per_page") limit : Int) : Single<List<PullRequest>>
}