package com.shashankk.github_pr.vm

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import androidx.recyclerview.widget.DiffUtil
import com.shashankk.github_pr.base.ViewProvider
import com.shashankk.github_pr.data.PullRequestDataSourceFactory

class MainActivityViewModel(
    val modelDiffUtil: DiffUtil.ItemCallback<PullRequestItemViewModel> = PullRequestItemViewModel.DIFF_UTIL,
    val screen: IMainActivityViewModel,
    val viewProvider: ViewProvider
) : ViewModel(),
    IPullRequestItemViewModel, INetworkCallback {

    val userName = MutableLiveData<String>().apply { value = "square" }
    val repo = MutableLiveData<String>().apply { value = "okHttp" }

    val errorVisibility = MutableLiveData<Boolean>().apply { value = false }
    val loaderVisibility = MutableLiveData<Boolean>().apply { value = false }

    val repoLink = MutableLiveData<String>().apply { value = "square/okHttp" }

    val onInputStringSubmit = {
        repoLink.value?.split("/")?.let { parts ->
            if (parts.size > 1) {
                userName.value = parts[0]
                repo.value = parts[1]
                bindData()
            }
        }
    }


    private fun bindData() {
        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(5)
            .build()

        val itemPagedList: LiveData<PagedList<PullRequestItemViewModel>> =
            LivePagedListBuilder(
                screen.getDataFactory(userName.value ?: "", repo.value ?: "", this, this),
                config
            ).build()

        screen.bindData(itemPagedList, modelDiffUtil, viewProvider)
    }

    override fun onDataFetching() {
        errorVisibility.value = false
    }

    override fun onError() {
        loaderVisibility.value = false
        errorVisibility.value = true
    }

    override fun onInitialLoading() {
        loaderVisibility.value = true
        errorVisibility.value = false
    }

    override fun onInitialLoadingComplete() {
        loaderVisibility.value = false
        errorVisibility.value = false
    }

    override fun onDataFetchComplete() {
        errorVisibility.value = false
    }

    fun onScreenCreated() {
        bindData()
    }
}

interface IMainActivityViewModel {
    fun getDataFactory(
        userName: String,
        repo: String,
        networkCallback: INetworkCallback,
        itemCallback: IPullRequestItemViewModel
    ): PullRequestDataSourceFactory

    fun bindData(
        source: LiveData<PagedList<PullRequestItemViewModel>>,
        diffUtil: DiffUtil.ItemCallback<PullRequestItemViewModel>,
        viewProvider: ViewProvider
    )
}

interface INetworkCallback {

    fun onInitialLoading()

    fun onInitialLoadingComplete()

    fun onDataFetching()

    fun onDataFetchComplete()

    fun onError()
}

