package com.shashankk.github_pr.vm

import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.DiffUtil
import com.shashankk.github_pr.R
import com.shashankk.github_pr.Utils
import com.shashankk.github_pr.model.PullRequest

class PullRequestItemViewModel(val pullRequest: PullRequest, callback: IPullRequestItemViewModel) : ViewModel() {

    val createdBy = "created by ${pullRequest.user?.login}"

    val formattedTime = Utils.getFormattedTime(pullRequest.updatedAt)

    val prNumber = "#${pullRequest.number}"

    val statusColor = when (pullRequest.state) {
        "open" -> "#0000FF"
        "rejected" -> "#FF0000"
        "merged" -> "#00FF00"
        "accepted" -> "#00FF00"
        else -> "#0000FF"
    }

    companion object {
        val DIFF_UTIL = object : DiffUtil.ItemCallback<PullRequestItemViewModel>() {
            override fun areItemsTheSame(
                oldItem: PullRequestItemViewModel,
                newItem: PullRequestItemViewModel
            ): Boolean {
                return oldItem.pullRequest.number == newItem.pullRequest.number
            }

            override fun areContentsTheSame(
                oldItem: PullRequestItemViewModel,
                newItem: PullRequestItemViewModel
            ): Boolean {
                return oldItem.pullRequest.updatedAt == newItem.pullRequest.updatedAt
            }
        }
    }
}

interface IPullRequestItemViewModel {

}