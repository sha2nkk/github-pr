package com.shashankk.github_pr

import android.text.format.DateUtils
import java.text.SimpleDateFormat
import java.util.*


object Utils {

    fun getFormattedTime(timeInString : String?) :String {
        if(timeInString==null) return ""
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
        sdf.timeZone = TimeZone.getTimeZone("GMT")
        val time = sdf.parse(timeInString).time
        val now = System.currentTimeMillis()

        return DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS).toString()
    }
}