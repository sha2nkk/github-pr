package com.shashankk.github_pr.data

import android.annotation.SuppressLint
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.PageKeyedDataSource
import com.shashankk.github_pr.base.GitHubApplication
import com.shashankk.github_pr.vm.INetworkCallback
import com.shashankk.github_pr.vm.IPullRequestItemViewModel
import com.shashankk.github_pr.vm.PullRequestItemViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable

class PullRequestDataSource(
    private var userName: String,
    private var repo: String,
    private val networkCallback: INetworkCallback,
    private val itemCallback: IPullRequestItemViewModel
) :
    PageKeyedDataSource<Int, PullRequestItemViewModel>() {

    private val disposables = ArrayList<Disposable>()

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, PullRequestItemViewModel>
    ) {
        Handler(Looper.getMainLooper()).post {
            networkCallback.onInitialLoading()
            networkCallback.onDataFetching()
        }
        disposables.add(GitHubApplication
            .getWebInterface()
            .getPullRequests(userName, repo, 1, 5)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { pullRequests, error ->
                if (error != null) {
                    networkCallback.onError()
                    return@subscribe
                }
                callback.onResult(pullRequests.map { PullRequestItemViewModel(it, itemCallback) }, null, 1)
                networkCallback.onInitialLoadingComplete()
                networkCallback.onDataFetchComplete()
            })
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, PullRequestItemViewModel>) {
        Log.d("Paging", "Fetching for Page:" + params.key)
        Handler(Looper.getMainLooper()).post {
            networkCallback.onDataFetching()
        }
        disposables.add(GitHubApplication
            .getWebInterface()
            .getPullRequests(userName, repo, params.key, 5)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { pullRequests, error ->
                if (error != null) {
                    networkCallback.onError()
                    return@subscribe
                }
                val nextKey = if (pullRequests.size < 5) null else params.key + 1
                callback.onResult(pullRequests.map { PullRequestItemViewModel(it, itemCallback) }, nextKey)
                networkCallback.onDataFetchComplete()
            })
    }

    @SuppressLint("CheckResult")
    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, PullRequestItemViewModel>) {
    }

    fun loadNewData(userName: String, repo: String) {
        this.userName = userName
        this.repo = repo
        invalidate()
    }

    override fun invalidate() {
        disposables.forEach { if (!it.isDisposed) it.dispose() }
        super.invalidate()

    }
}

class PullRequestDataSourceFactory(
    val userName: String,
    val repo: String,
    val networkCallback: INetworkCallback,
    val itemCallback: IPullRequestItemViewModel
) :
    DataSource.Factory<Int, PullRequestItemViewModel>() {

    private val pullRequestLiveDataSource = MutableLiveData<PageKeyedDataSource<Int, PullRequestItemViewModel>>()

    override fun create(): DataSource<Int, PullRequestItemViewModel> {
        val datasource = PullRequestDataSource(userName, repo, networkCallback, itemCallback)
        pullRequestLiveDataSource.postValue(datasource)
        return datasource
    }

    fun getPullRequestLiveDataSource(): MutableLiveData<PageKeyedDataSource<Int, PullRequestItemViewModel>> {
        return pullRequestLiveDataSource
    }

}
