package com.shashankk.github_pr

import android.annotation.SuppressLint

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.shashankk.github_pr.base.BasePagedListAdapter
import com.shashankk.github_pr.base.GitHubApplication
import com.shashankk.github_pr.base.ViewProvider
import com.shashankk.github_pr.data.PullRequestDataSourceFactory
import com.shashankk.github_pr.databinding.ActivityMainBinding
import com.shashankk.github_pr.model.PullRequest
import com.shashankk.github_pr.vm.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MainActivity : AppCompatActivity(), IMainActivityViewModel {

    lateinit var viewModel: MainActivityViewModel
    lateinit var binding: ActivityMainBinding

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        viewModel = MainActivityViewModel(PullRequestItemViewModel.DIFF_UTIL, this, object : ViewProvider {

            override fun getLifeCycleOwner() = this@MainActivity

            override fun getLayout(model: Class<out ViewModel>?): Int {
                return when (model) {
                    PullRequestItemViewModel::class -> R.layout.item_pull_request
                    else -> R.layout.item_pull_request
                }
            }
        })
        binding.vm = viewModel;
        binding.setLifecycleOwner(this)
        binding.executePendingBindings()
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        viewModel.onScreenCreated()
    }

    override fun bindData(
        source: LiveData<PagedList<PullRequestItemViewModel>>,
        diffUtil: DiffUtil.ItemCallback<PullRequestItemViewModel>,
        viewProvider: ViewProvider
    ) {
        val adapter = BasePagedListAdapter(viewProvider, diffUtil)

        binding.rvItems.layoutManager = LinearLayoutManager(binding.rvItems.context, RecyclerView.VERTICAL, false)
        binding.rvItems.adapter = adapter
        binding.executePendingBindings()
        source.observe(viewProvider.getLifeCycleOwner(), Observer<PagedList<PullRequestItemViewModel>> {
            adapter.submitList(it)
        })
    }

    override fun getDataFactory(
        userName: String,
        repo: String,
        networkCallback: INetworkCallback,
        itemCallback: IPullRequestItemViewModel
    ) = PullRequestDataSourceFactory(userName, repo, networkCallback, itemCallback)

}
