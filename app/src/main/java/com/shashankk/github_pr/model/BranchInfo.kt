package com.shashankk.github_pr.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class BranchInfo {
    @SerializedName("label")
    @Expose
    var label: String? = null
    @SerializedName("ref")
    @Expose
    var ref: String? = null
}