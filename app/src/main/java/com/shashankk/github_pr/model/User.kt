package com.shashankk.github_pr.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class User {

    @SerializedName("node_id")
    @Expose
    var nodeId: String? = null
    @SerializedName("avatar_url")
    @Expose
    var avatarUrl: String? = null
    @SerializedName("url")
    @Expose
    var url: String? = null
    @SerializedName("type")
    @Expose
    var type: String? = null
    @SerializedName("login")
    @Expose
    var login: String? = null

}