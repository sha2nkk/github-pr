package com.shashankk.github_pr.model

import androidx.recyclerview.widget.DiffUtil
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PullRequest {

    @SerializedName("node_id")
    @Expose
    var nodeId: String? = null
    @SerializedName("url")
    @Expose
    var url: String? = null
    @SerializedName("title")
    @Expose
    var title: String? = null
    @SerializedName("number")
    @Expose
    var number: Int? = null
    @SerializedName("state")
    @Expose
    var state: String? = null
    @SerializedName("body")
    @Expose
    var body: String? = null
    @SerializedName("user")
    @Expose
    var user: User? = null
    @SerializedName("created_at")
    @Expose
    var createdAt: String? = null
    @SerializedName("updated_at")
    @Expose
    var updatedAt: String? = null
    @SerializedName("merged_at")
    @Expose
    var mergedAt: String? = null
    @SerializedName("merge_commit_sha")
    @Expose
    var mergeCommitSha: String? = null
    @SerializedName("assignee")
    @Expose
    var assignee: User? = null
    @SerializedName("requested_reviewers")
    @Expose
    var requestedReviewers: List<User>? = null
    @SerializedName("base")
    @Expose
    var base: BranchInfo? = null
    @SerializedName("head")
    @Expose
    var head: BranchInfo? = null
}