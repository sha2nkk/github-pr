package com.shashankk.github_pr.base


import androidx.annotation.IntegerRes
import androidx.annotation.LayoutRes
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel

interface ViewProvider {

    fun getLifeCycleOwner(): LifecycleOwner

    @LayoutRes
    fun getLayout(model: Class<out ViewModel>?): Int
}