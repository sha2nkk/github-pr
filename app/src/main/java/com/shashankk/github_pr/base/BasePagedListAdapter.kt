package com.shashankk.github_pr.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.shashankk.github_pr.BR

class BasePagedListAdapter<T : ViewModel>(val viewProvider: ViewProvider, diffUtil: DiffUtil.ItemCallback<T>) :
    PagedListAdapter<T, BasePagedListAdapter.ViewHolder>(diffUtil) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val holder = ViewHolder(DataBindingUtil.inflate(inflater, viewType, parent, false))
        return holder
    }

    override fun getItemViewType(position: Int): Int {
        val item = getItem(position)
        return viewProvider.getLayout(if (item == null) null else item::class.java)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       getItem(position)?.let {
           holder.binding.setVariable(BR.vm, it)
           holder.binding.setLifecycleOwner(viewProvider.getLifeCycleOwner())
           holder.binding.executePendingBindings()
       }
    }


    class ViewHolder(val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root)
}