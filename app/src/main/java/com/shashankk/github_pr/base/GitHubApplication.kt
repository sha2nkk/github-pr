package com.shashankk.github_pr.base

import android.app.Application
import com.shashankk.github_pr.api.GitHubApi
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class GitHubApplication : Application() {

    val apiInterface: GitHubApi by lazy {
        Retrofit.Builder()
            .baseUrl("https://api.github.com")
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(com.shashankk.github_pr.api.GitHubApi::class.java)
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    companion object {

        private lateinit var instance: GitHubApplication

        fun getInstance() = instance

        fun getWebInterface() = instance.apiInterface
    }
}