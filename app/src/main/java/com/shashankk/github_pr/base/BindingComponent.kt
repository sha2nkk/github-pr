package com.shashankk.github_pr.base


import android.graphics.Color
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.paging.PagedList
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import java.lang.Exception

object BindingComponent {

    @JvmStatic
    @BindingAdapter("textColor")
    fun bindTextColor(tv: TextView, color: String?) {
        color?.let {
            try {
                tv.setTextColor(Color.parseColor(color))
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    @JvmStatic
    @BindingAdapter("background")
    fun bindbackground(v: View, color: String?) {
        color?.let {
            try {
                v.setBackgroundColor(Color.parseColor(color))
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    @JvmStatic
    @BindingAdapter("urlImage")
    fun bindImageUrl(iv: ImageView, url: String?) {
        url?.let {
            Glide.with(iv.context)
                .load(url)
                .into(iv)
        }
    }

    @JvmStatic
    @BindingAdapter("onClick")
    fun bindClick(v: View, callback: () -> Unit?) {
        v.setOnClickListener {
            callback()
        }
    }

    @JvmStatic
    @BindingAdapter("android:visibility")
    fun bindVisibility(v: View, value: Boolean?) {
        value?.let {
            v.visibility = if (value) View.VISIBLE else View.GONE
        }
    }
}